#!/usr/bin/env bash
ScriptPath="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
root=`sed 's/\/zscaler-scripts\/dev//g'<<< "${ScriptPath}"`
# comment/uncomment below line for importing fresh drupal8 sql sccript
# mysql apex8_zscaler_test < "${root}"/zscaler-scripts/dev/apex8_zscaler_test.sql
# comment/uncomment below line for importing fresh drupal 8  sql sccript
#mysql apex8_zscaler_test < "${root}"/zscaler-scripts/dev/apex8_zscaler_test_after_migration.sql
mysql apex8_zscaler_test < "${root}"/zscaler-scripts/dev/drupal-security-update-24-feb-2020/apex8_zscaler_test_24-feb-2020.sql

exit 0