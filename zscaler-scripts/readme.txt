this file describes the usage of scripts files in this folder

change your directory to zscaler-scripts folder

the scripts can be used as required in the following order

#setup mysql user "zscaler", for first time users only
./setup-user.sh

#setup mysql database "zscaler_marketing"
./setup-db.sh

#copy database from production to your local machine
./update-local.sh
