<?php

/**
 * Each developer should include custom settings here. This file is not commmited to Git and will not go to the server.
 */

$databases = array (
    'default' =>
        array (
            'default' =>
                array (
                    'database' => 'apex3_zscaler_com',
                    'username' => 'zscaler',
                    'password' => 'abc123',
                    'host' => '127.0.0.1',
                    'port' => '',
                    'driver' => 'mysql',
                    'prefix' => '',
                ),
        ),
);

$config_directories['sync'] = 'sites/default/files/config_0LHsO51hKXjnx7ig32V1xVCjzmLgFCjzste8MWqFtTB9X5Z53VZK8DASvA8cgizDVPAC4-GU0w/sync';

// disable caches
$conf['php_storage']['default']['class'] = 'Drupal\Component\PhpStorage\FileStorage';
$conf['cache_classes']['cache'] = 'Drupal\Core\Cache\NullBackend';

$config['system.logging']['error_level'] = 'verbose';

/**
 * Twig Settings
 */
//$settings['twig_debug'] = TRUE;


/**
 * Disable the render cache (this includes the page cache).
 *
 * Note: you should test with the render cache enabled, to ensure the correct
 * cacheability metadata is present. However, in the early stages of
 * development, you may want to disable it.
 *
 * This setting disables the render cache by using the Null cache back-end
 * defined by the development.services.yml file above.
 *
 * Do not use this setting until after the site is installed.
 */
//$settings['cache']['bins']['render'] = 'cache.backend.null';


/**
 * Disable Dynamic Page Cache.
 *
 * Note: you should test with Dynamic Page Cache enabled, to ensure the correct
 * cacheability metadata is present (and hence the expected behavior). However,
 * in the early stages of development, you may want to disable it.
 */
//$settings['cache']['bins']['dynamic_page_cache'] = 'cache.backend.null';

if (file_exists($app_root . '/' . $site_path . '/settings.local.php')) {
  include $app_root . '/' . $site_path . '/settings.local.php';
}