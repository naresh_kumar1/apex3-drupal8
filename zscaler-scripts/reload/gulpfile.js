
const gulp = require('gulp');
const notify = require('gulp-notify');
const livereload = require('gulp-livereload');
const print = require('gulp-print');
const runSequence = require('run-sequence');

const pathsYouAreWorking = [
  '/private/var/www/apex3.zscaler.com/docroot/modules/custom/**/*',
  '/private/var/www/apex3.zscaler.com/docroot/themes/custom/zscaler/includes/**/*',
  '/private/var/www/apex3.zscaler.com/docroot/themes/custom/zscaler/js/src/**/*',
  '/private/var/www/apex3.zscaler.com/docroot/themes/custom/zscaler/templates/**/*'
];

gulp.task('default', function () {
  // livereload.listen();
  livereload.listen({
    port:35729,
    host:'apex3.zscaler.test',
    quite: true,
    // reloadPage:'/',
  });
  // gulp.watch(pathsYouAreWorking, ['reload']);
  gulp.watch(pathsYouAreWorking, livereload.reload);


});

gulp.task('reload', function () {
  return gulp.src(pathsYouAreWorking)
    //.pipe(print())
    .pipe(livereload({quite: true}));
    //.pipe(notify('reloaded'));
});
