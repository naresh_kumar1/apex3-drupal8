#!/usr/bin/env bash
echo -n "This script require you to enter mysql root password. Contine (y/n)?"
read answer
if [ ${answer} == "y" ]; then
    mysql -uroot -p <<EOF
    GRANT ALL ON *.* TO 'zscaler'@'localhost' IDENTIFIED BY 'abc123' WITH GRANT OPTION;
    FLUSH PRIVILEGES;
    #SHOW GRANTS
    SHOW GRANTS FOR 'zscaler'@'localhost';
    SELECT * FROM mysql.user WHERE (User='zscaler' OR User='root') AND Host='localhost';
EOF
fi
exit 0
