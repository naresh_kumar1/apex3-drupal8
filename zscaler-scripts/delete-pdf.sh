#!/usr/bin/env bash

PdfPathOnServer="/home/zscaler/prod/sites/default/files"
#PdfPathOnServer="/var/www/zscaler-marketing/docroot/sites/default/files"
filename="$1"
while read -r line
do
    name="$line"
    pdfFile="${PdfPathOnServer}${name}"
    if [ -f "${pdfFile}" ]; then
#      printf "\n File exists ${pdfFile}"
      printf "\n"
      printf "\n File exists"
      printf "\n moved to archive" && \
      mv ${pdfFile} "${PdfPathOnServer}/pdf/archive/"
    else
      printf "\n\t\tdoes not exists ${pdfFile}"
    fi
done < "$filename"